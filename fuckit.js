const Telegraf = require('telegraf');
const bot = new Telegraf('643796017:AAGOJyn6hg3gmqCaVBQE1LZViDNwvK55lyU');
const session = require('telegraf/session');
const Composer = require('telegraf/composer');
const TelegrafInlineMenu = require('telegraf-inline-menu');
const stepHandler = new Composer();
const Stage = require('telegraf/stage');
const WizardScene = require('telegraf/scenes/wizard');
// const userController = require('../controllers/user.controller');

bot.use(session());

const main = new TelegrafInlineMenu("Welcome.");
main.setCommand('start');
main.simpleButton('Start Survey', 'START_SURVEY', {
  doFunc: async ctx => surveyController.AddProject(ctx, bot)
});


module.exports = {

  AddProject: async function (ctx, bot) {

    const superWizard = new WizardScene('super-wizard',
      (ctx) => {
        ctx.reply('Step 1', Markup.inlineKeyboard([
          Markup.urlButton('❤️', 'http://telegraf.js.org'),
          Markup.callbackButton('➡️ Next', 'next'),
        ]).extra());
        return ctx.wizard.next();
      },
      (ctx) => {
        ctx.reply('Step 2');
        return ctx.wizard.next();
      },
      (ctx) => {
        ctx.reply('Done');
        return ctx.wizard.leave();
      },
    );

    const stage = new Stage([superWizard]);
    bot.use(stage.middleware());
    Stage.enter('super-wizard');
  },
};
